<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Cobalt
 */

// Set the archive layout from here.
$cobalt_archive_layout = esc_attr ( get_theme_mod( 'cobalt_archive_layout' ) );
if ( empty( $cobalt_archive_layout ) ) {
	$cobalt_archive_layout = 'default';
}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' ); ?>>

	<?php if ( is_sticky() ) : ?>
		<span class="sticky-post"><?php _e( 'Sticky', 'cobalt' ); ?></span>
	<?php endif; ?>

<?php if ( is_single() || 'default' === $cobalt_archive_layout  ) : ?>

	<div class="entry-padding-area">
		<header class="entry-header">
			<div class="entry-categories">
				<?php the_category(); ?>
			</div><!-- .entry-categories -->
			<?php
				if ( is_single() ) {
					the_title( '<h1 class="entry-title">', '</h1>' );
				} else {
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}
			?>
		</header><!-- .entry-header -->

		<?php cobalt_featured_image( 'featured' ); ?>

		<div class="entry-content">
			<?php
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'cobalt' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cobalt' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
	</div><!-- .entry-padding-area -->

	<div class="entry-padding-area footer">
		<footer class="entry-footer">
			<?php cobalt_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</div><!-- .entry-padding-area footer -->
</article><!-- #post-## -->

<?php else : ?>

<div class="entry-padding-area two-columns">

		<?php if ( has_post_thumbnail() ) : ?>
			<div class="entry-compact-left">
				<?php cobalt_featured_image( 'featured' ); ?>
			</div>
		<?php endif; ?>

		<div class="entry-compact-right">
				<header class="entry-header">
					<div class="entry-categories">
						<?php the_category(); ?>
					</div><!-- .entry-categories -->
					<?php
						if ( is_single() ) {
							the_title( '<h1 class="entry-title">', '</h1>' );
						} else {
							the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						}
					?>
				</header><!-- .entry-header -->

				<div class="entry-excerpt">
					<?php	the_excerpt(); ?>
				</div><!-- .entry-content -->

				<a class="continue-reading" href="<?php the_permalink(); ?>"><?php _e( 'Continue reading', 'cobalt' ); ?> <i class="fa fa-chevron-right"></i></a>
			</div><!-- .entry-padding-area -->
		</div>
</article><!-- #post-## -->

<?php endif; ?>