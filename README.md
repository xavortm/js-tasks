# Cobalt Free WordPress theme

Designed and developed by Alex Dimitrov @xavortm and the DevriX team. The theme suppports all Jetpack widets and has custom made stylings for them.

---

Cobalt has custom build in support for:

* BBPress forums and widgets
* JetPack functionality and widgets
