=== Cobalt ===

Contributors: xavortm, devrix
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.8
Tested up to: 4.8.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Cobalt is a theme build to look and feel as a premium quality one. Modern look, fast and clean code and good support. The theme supports all Jetpack widgets and has custom made stylings for them.

== Description ==

A clean, straight to the point blogging WordPress theme. It uses native fonts, just like the WordPress dashboard, but also provides a serif fonts through the customizer. The theme can achieve 100/100 page score on a good server setup. We are planning to release more updates for it to support even more of the top used plugins. 

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Cobalt includes support for Infinite Scroll in Jetpack.

== Changelog ==

= 1.0 - August 23 2016 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* Fork of https://github.com/devrix/dx-starter/, (C) 2010-2016 DevriX, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
* Foundation http://foundation.zurb.com/, (C) 1998‐2016 ZURB, Inc, [MIT](http://opensource.org/licenses/MIT)
* Font Awesome licensed under [SIL OFL 1.1](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)
* Font Awesome Code licensed under [MIT](http://opensource.org/licenses/MIT)
* "Cyclist" photo on screenshot.png by David Marcu https://unsplash.com/photos/VfUN94cUy4o [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
