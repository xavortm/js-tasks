<?php

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since DX Starter 1.0.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function cobalt_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		  is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'cobalt_post_thumbnail_sizes_attr', 10 , 3 );


/**
 * Prints out better responsive images. Uses the class "responsive-image"
 *
 * $url The image address
 * $size Name of the thumbnail size
 * $class Custom image class
 * $alt Alternative text if it can't be loaded
 *
 * @since DX Starter v1.1.0
 */
function cobalt_featured_image( $size = 'featured', $class = '', $attr = '', $echo = true ) {
	do_action( 'cobalt_before_featured_image_func' );

	if ( ! $echo ) {
		return;
	}

	if ( ! has_post_thumbnail() ) {
		return;
	}

	if ( empty( $size ) ) {
		return;
	}

	if ( empty( $class ) ) {
		$class = '';
	}


	// Do not print it in the content if it's already in the header
	global $post;
	$page_thumbnail_large = get_the_post_thumbnail_url( $post->ID, 'page-thumbnail' );
	$page_thumbnail_size = cobalt_has_large_featured_image( $post->ID );

	if ( 'large' === $page_thumbnail_size ) {
		return;
	}
	
	if ( ! is_single() ) {
		echo '<a href="' . get_the_permalink() . '">';
	}

	// Print the HTML of the image
	echo "<picture class='entry-featured-image ${class}'>";

	// use the standard featured image function
	the_post_thumbnail( $size, $attr );

	echo "</picture>";

	if ( ! is_single() ) {
		echo '</a>';
	}
}

/**
 * We are using very wide image size. If they don't have such do not show it, instead
 * print the featured image in the entry's post content. We will have default background
 * set from the CSS of the theme.
 */
function cobalt_has_large_featured_image( $post_ID ) {

	// Grab the width of the page thumbnail.
	$image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post_ID ), "featured-large" ); 
	$image_width = $image_data[1];

	// If the image they've provided isn't big enough use the smaller one in the content.
	if ( 1920 != $image_width ) {
		return 'normal';
	}

	return 'large';
}

/**
 * Prints header image on top of the page. If on single post with thumbnail that
 * is big enough then print that. Else default back to the header image.
 * 
 * @param  	[string] $location 'before-menu' or 'after-menu'
 * @todo  	optimize the image file loading
 * @todo  	make sure that $post exists before using it
 */
function cobalt_header_image( $location ) {
	global $post;
	$has_image = has_header_image();

	if ( function_exists( 'is_woocommerce' ) ) {
		if ( is_woocommerce() ) {
			return;	
		}
	}

	// Don't do this in WooCommerce
	if ( true === $has_image ) {
		$header_image_url = get_header_image();
	} 

	// If the header image is not set get out of here :)
	if ( is_singular() && has_post_thumbnail( $post->ID ) ) {
		$page_thumbnail_large = get_the_post_thumbnail_url( $post->ID, 'featured-large' );
		$page_thumbnail_size = cobalt_has_large_featured_image( $post->ID );

		if ( 'large' === $page_thumbnail_size ) {
			$header_image_url = $page_thumbnail_large;
		}
	}

	if ( ! empty( $post ) ) {
		$get_post = get_post( get_post_thumbnail_id( $post->ID ) );
		
		if ( is_object( $get_post ) ) {
			$header_image_caption = $get_post->post_excerpt;
		}
	}

	$header_image_title = get_bloginfo( 'title' );
	
	// This is the final part - just print it on the page.
	if ( empty( get_theme_mod( 'cobalt_header_image_position' ) ) ) {
		$position = 'after-menu';
	}else{
		$position = get_theme_mod( 'cobalt_header_image_position' );
	}

	if ( $has_image && $location === esc_attr ( $position ) ) {
		?>
		<div class="header-featured-image responsive-image">
			<img src="<?php echo $header_image_url; ?>" alt="<?php $header_image_url; ?>" />
			
			<?php if ( ! empty ( $header_image_caption ) && is_singular() ) : ?>
				<div class="caption-wrapper">
					<span class="caption"><?php echo $header_image_caption ?></span>
				</div>
			<?php endif; ?>

		</div><!-- /end header-featured-image -->
		<?php
	}
}

