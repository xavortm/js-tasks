<?php
/**
 * Sample implementation of the Custom Header feature.
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php if ( get_header_image() ) : ?>
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
		<img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="">
	</a>
	<?php endif; // End header image check. ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package Cobalt
 */

/**
 * Sample implementation of the Custom Header feature.
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package Cobalt
 */
function cobalt_customiser_settings( $wp_customize ) {

	// Remove the default color settings in order to keep everything
	// in one place and easy to access _for the user_
	$wp_customize->remove_section( 'colors' );

	// header_image
	$wp_customize->add_setting( 'cobalt_header_image_position' , array(
		'default'     => 'after-menu',
		'transport'   => 'refresh',
		'sanitize_callback' => 'cobalt_sanitize_choices'
	) );

	$wp_customize->add_control(
		'cobalt_header_image_position', array(
			'label'     => __( 'Header image position', 'cobalt' ),
			'description' => __( 'Show the header image before or after the menu.', 'cobalt' ),
			'section'   => 'header_image',
			'settings'  => 'cobalt_header_image_position',
			'type'     	=> 'radio',
			'choices'	=> array(
				'after-menu' => 'After header',
				'before-menu' => 'Before header',
			)
		)
	);

	$wp_customize->add_section( 'cobalt_settings' , array(
		'title'      => __( 'Cobalt theme settings', 'cobalt' ),
		'priority'   => 30,
	) );

	$wp_customize->add_setting( 'cobalt_primary_color' , array(
		'default'     => '#00A7FF',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color'
	) );

	$wp_customize->add_setting( 'cobalt_background_color' , array(
		'default'     => '#F5F5F5',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize,
		'cobalt_primary_color', array(
			'label'      => __( 'Primary color', 'cobalt' ),
			'section'    => 'cobalt_settings',
			'settings'   => 'cobalt_primary_color',
		)
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize,
		'cobalt_background_color', array(
			'label'      => __( 'Background color', 'cobalt' ),
			'section'    => 'cobalt_settings',
			'settings'   => 'cobalt_background_color',
		)
	) );

	$wp_customize->add_setting( 'cobalt_archive_layout' , array(
		'type' 			=> 'theme_mod',
		'transport'   	=> 'refresh',
		'capability' 	=> 'edit_theme_options',
		'default'     	=> 'default',
		'sanitize_callback' => 'cobalt_sanitize_choices'
	));

	$wp_customize->add_control(
		'cobalt_archive_layout', array(
			'label'     => __( 'Archive layout', 'cobalt' ),
			'description' => __( 'How to list your articles. Compact will show small image with less information.', 'cobalt' ),
			'section'   => 'cobalt_settings',
			'settings'  => 'cobalt_archive_layout',
			'type'     	=> 'radio',
			'choices'	=> array(
				'default' => 'Default', // This is the default one
				'compact' => 'Compact'
			),
		)
	);

	$wp_customize->add_setting( 'cobalt_typography_type' , array(
		'type' 			=> 'theme_mod',
		'transport'   	=> 'refresh',
		'capability' 	=> 'edit_theme_options',
		'default'    	=> 'sans',
		'sanitize_callback' => 'cobalt_sanitize_choices'
	));

	$wp_customize->add_control(
		'cobalt_typography_type', array(
			'label'     => __( 'Set typography type', 'cobalt' ),
			'description' => __( 'Book reading is font good for long reading sessions. It will change only the content of your articles, the rest will remain unchanged.', 'cobalt' ),
			'section'   => 'cobalt_settings',
			'settings'  => 'cobalt_typography_type',
			'type'     	=> 'radio',
			'choices'	=> array(
				'sans-serif' => 'Book reading',
				'sans' => 'System fonts',
			)
		)
	);

	$wp_customize->add_setting( 'cobalt_sidebar_position' , array(
		'type' 			=> 'theme_mod',
		'transport'   	=> 'refresh',
		'capability' 	=> 'edit_theme_options',
		'default'     	=> 'right',
		'sanitize_callback' => 'cobalt_sanitize_choices'
	));

	$wp_customize->add_control(
		'cobalt_sidebar_position', array(
			'label'     => __( 'Sidebar position', 'cobalt' ),
			'description' => __( 'Chose between left, right and no sidebar.', 'cobalt' ),
			'section'   => 'cobalt_settings',
			'settings'  => 'cobalt_sidebar_position',
			'type'     		=> 'radio',
			'choices'	=> array(
				'right' => 'Right sidebar',
				'left' => 'Left sidebar',
				'none' => 'No sidebar',
			),
		)
	);

	$wp_customize->add_setting( 'cobalt_spacings' , array(
		'type' 			=> 'theme_mod',
		'transport'   	=> 'refresh',
		'capability' 	=> 'edit_theme_options',
		'default'     	=> 'spacing-cozy',
		'sanitize_callback' => 'cobalt_sanitize_choices'
	));

	$wp_customize->add_control(
		'cobalt_spacings', array(
			'label'     => __( 'White space', 'cobalt' ),
			'description' => __( 'How much white space you want on your site? Different settings will allow more or less content to be visible on one scroll.', 'cobalt' ),
			'section'   => 'cobalt_settings',
			'settings'  => 'cobalt_spacings',
			'type'     	=> 'radio',
			'choices'	=> array(
				'spacing-cozy' => 'Cozy', // This is the default one
				'spacing-moderate' => 'Moderate'
			),
		)
	);

}
add_action( 'customize_register', 'cobalt_customiser_settings' );

function cobalt_sanitize_choices( $input, $setting ) {
    global $wp_customize;

    $control = $wp_customize->get_control( $setting->id );

    if ( array_key_exists( $input, $control->choices ) ) {
        return esc_attr( $input );
    } else {
        return esc_attr( $setting->default );
    }
}

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses cobalt_header_style()
 */
function cobalt_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'cobalt_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1920,
		'height'                 => 500,
		'flex-height'            => true,
		'wp-head-callback'       => 'cobalt_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'cobalt_custom_header_setup' );

if ( ! function_exists( 'cobalt_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog.
 *
 * @see cobalt_custom_header_setup().
 * @see https://codex.wordpress.org/Theme_Customization_API
 */
function cobalt_header_style() {
	$header_text_color = get_header_textcolor();

	$cobalt_primary_color = esc_attr ( get_theme_mod( 'cobalt_primary_color', '#00A7FF' ) );
	$cobalt_background_color = esc_attr ( get_theme_mod( 'cobalt_background_color', '#f0f0f0' ) );

	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
	?>
		.site-title,
		.site-description {
			position: absolute;
			clip: rect(1px, 1px, 1px, 1px);
		}
	<?php
		// If the user has set a custom color for the text use that.
		else :
	?>
		.site-title a,
		.site-description,
		.main-navigation .menu-item a,
		.main-navigation .page_item a,
		.main-navigation .menu-item a a,
		.main-navigation .page_item a a,
		.main-navigation .menu-toggle {
			color: #<?php echo esc_attr ( $header_text_color ) ?>;
		}
	<?php endif; ?>

	<?php if ( 'sans-serif' === get_theme_mod( 'cobalt_typography_type' ) ) : ?>
		.entry .entry-content {
			font-family: "Book Antiqua", Palatino, "Palatino Linotype", "Palatino LT STD", Georgia, serif;;
		}

		.entry .entry-content h1,
		.entry .entry-content h2,
		.entry .entry-content h3,
		.entry .entry-content h4,
		.entry .entry-content h5 {
			font-family: "Merriweather", "Book Antiqua", Palatino, "Palatino Linotype", "Palatino LT STD", Georgia, serif;;
		}
	<?php endif; ?>

	<?php if ( 'left' === get_theme_mod( 'cobalt_sidebar_position' ) ) : ?>
		.section-main > .row { flex-direction: row-reverse; }
	<?php endif; ?>

	<?php if ( 'none' === get_theme_mod( 'cobalt_sidebar_position' ) ) : ?>
		.section-main > .row > .medium-4 { display: none; }
		.section-main > .row > .medium-8 {
			flex: 1 1 auto;
			max-width: 800px;
			margin: 0 auto;
		}
	<?php endif; ?>

	body {
		background-color: <?php echo $cobalt_background_color ?>;
	}

	.button,
	.entry .read-more,
	.comments-area .form-submit .submit,
	.page-content .read-more,
	.widget_contact_info .confit-phone,
	.entry .sticky-post,
	.page-content .sticky-post,
	.comments-area .logged-in-as,
	.product span.onsale {
		background-color:<?php echo $cobalt_primary_color ?>;
	}

	a,
	.entry-content a,
	.main-navigation .menu-toggle:hover,
	.main-navigation .menu-item a:hover,
	.sub-menu a:hover {
		color:<?php echo $cobalt_primary_color ?>;
	}

	.button,
	.entry .read-more,
	.comments-area .form-submit .submit,
	.page-content .read-more,
	.paged .nav-links .nav-next,
	.paged .nav-links .nav-previous,
	.entry.sticky,
	.sticky.page-content {
		border-color: <?php echo $cobalt_primary_color ?>;
	}

	.bbp_widget_login .bbp-login-form fieldset {
		box-shadow: 0 -5px <?php echo $cobalt_primary_color ?>;
	}

	@media (max-width: 860px) {
		.widget-area .widget_search {
			background-color: <?php echo $cobalt_primary_color ?>;
		}
	}

	</style>
	<?php
}
add_action( 'wp_head', 'cobalt_header_style' );
endif;

// Add the padidng setting to the body. The stylings are added from the
// Sass code only, nothing from here.
function cobaly_body_classes( $classes ) {
    $classes[] = get_theme_mod( 'cobalt_spacings' );
    return $classes;
}
add_filter( 'body_class','cobaly_body_classes' );
