<?php
/**
 * Template Name: Homepage
 *
 * @package Cobalt
 */

get_header(); ?>

<section class="section-fullwidth section-welcome">
	<div class="row">
		<div class="columns small-12">
			<?php // These are styled from CSS only ?>
			<aside id="homepage-widgets" class="homepage-widgets" role="complementary">
				<?php dynamic_sidebar( 'homepage-widgets' ); ?>
			</aside><!-- #secondary -->
		</div>
	</div>
</section>

<section class="section-fullwidth section-main">
	<div class="row">
		<div class="columns small-12 medium-8">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">

				<?php

					$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
					$default_posts_per_page = get_option( 'posts_per_page' );

					$args = array(
						'post_type' => 'post',
						'posts_per_page' => $default_posts_per_page,
						'paged' => $paged
					);
			    $post_query = new WP_Query($args);

					if ( $post_query->have_posts() ) : while( $post_query->have_posts() ) : $post_query->the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() );

					endwhile;

					// check if the max number of pages is greater than 1 
					if ( $post_query->max_num_pages > 1 ) : ?>
					  
					  <nav class="navigation post-navigation" role="navigation">
					  	<h2 class="screen-reader-text"><?php _e( 'Pages navigation', 'cobalt' ) ?></h2>
					    <div class="nav-links">
						    <div class="nav-previous">
						      <?php echo get_next_posts_link( __('Older Entries', 'cobalt'), $post_query->max_num_pages ); ?>
						    </div>
						    <div class="nav-next">
						      <?php echo get_previous_posts_link( __( 'Newer Entries', 'cobalt' ) ); ?>
						    </div>
					    </div>
					  </nav>

					<?php endif;

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>

				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .columns medium-8 -->
		<div class="columns small-12 medium-4">
			<?php get_sidebar(); ?>
		</div><!-- .columns medium-4 -->
	</div><!-- .row -->
</section><!-- .section-fullwidth section-main -->

<?php get_footer(); ?>